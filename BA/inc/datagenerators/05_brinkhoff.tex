\subsection{Brinkhoff}
Der netzwerkbasierte Generator von Thomas Brinkhoff\cite{brinkhoff2003generating} basiert auf der Beobachtung, dass 
Objekte ihre Bewegung oft an einem Netzwerk orientieren. Dies gilt für Straßen- und Zugverkehr. Flugverkehr folgt einem Netzwerk aus Luftkorridoren und Schiffverkehr benötigt ein Netzwerk aus Flüssen/Kanälen um sich fortzubewegen. Tiere folgen einem unsichtbaren Netzwerk während ihrer Abwanderung. Demnach kann fast kein Objekt außerhalb eines Netzwerks beobachtet werden.

Der Generator basiert auf einem diskreten Zeitmodell, bei dem die gesamte Simulationszeit in $n$ Zeitpunkte unterteilt wird. Zu jedem Zeitpunkt werden neue Objekte erstellt und existierende bewegt oder gelöscht, weil sie ihr Ziel erreicht haben. Jedes bewegliche Objekt gehört einer Klasse an, die seine Bewegung beschreibt. Diese Klasse beschreibt unter anderem die maximale Geschwindigkeit die ein Objekt annehmen kann. 

Der Generator kann das zugrundeliegende Netzwerk aus unterschiedlichen Quellen beziehen. Eine mögliche Quelle sind TIGER/Line-Dateien \cite{tigerLineWebsite} die Geoinformationen über die USA liefern. Diese werden vom United States Census Bureau bereitgestellt. Des Weiteren können Mapinfo Data Interchange (MIF)-Dateien importiert werden. Mapinfo Professional \cite{mapinfoReleasenotes} bietet hierbei die Möglichkeit unter anderem spatiale Datenbanken oder webbasierte Karten, wie OpenStreetMap Kartendaten, im MIF-Format zu importieren. 

Die Kanten des Netzwerks gehören einer Kantenklasse an, die das Geschwindigkeitslimit sowie die Kapazität der Kante beschreiben. Wenn die Anzahl an Objekten, die die Kante überqueren, die Kapazität übersteigt, sinkt das Geschwindigkeitslimit.
Des Weiteren können externe Objekte den Einfluss von Wetterbedingungen oder Ähnlichem beschreiben, welches sich ebenfalls auf die Geschwindigkeit auswirkt.

Die Erstellung neuer Objekte an bestimmten Orten ist zeitabhängig, welches die Modellierung von Pendlern zu Stoßzeiten ermöglicht. Da ein Neuberechnen von Routen zeitaufwendig ist, wird eine Route bei der Erstellung berechnet, bei einer längeren Reisezeit oder dem Eintreten eines Staus erneut evaluiert und gegebenenfalls neu berechnet.

\subsubsection{Aussagen}
Brinkhoff definiert neun Aussagen\cite{brinkhoff2002framework} anhand denen er die Bewegung von Objekten innerhalb von Netzwerken ableitet:
\begin{itemize}
	\item \textbf{Aussage 1} Sich bewegende reale Objekte folgen oft einem Netzwerk.
	\item \textbf{Aussage 2} Die meisten sich bewegenden Objekte nutzen einen schnellen Weg zu ihrem Ziel.
	\item \textbf{Aussage 3} Der Straßentyp hat einen direkten Einfluss auf die Bewegung(-sgeschwindigkeit) eines Objekts.
	\item \textbf{Aussage 4} Die Anzahl der sich bewegenden Objekte beeinflusst die Flussgeschwindigkeit negativ, falls ein Schwellenwert überschritten wird.
	\item \textbf{Aussage 5} Der Pfad eines sich bewegenden Objekts verändert sich eventuell, wenn sich die Geschwindigkeit auf einem Streckenabschnitt verändert.
	\item \textbf{Aussage 6} Die Anzahl von sich bewegenden Objekten ist eine zeitabhängige Funktion (Stoßzeiten/Wochenende/Ferien/Wetter)
	\item \textbf{Aussage 7} Die Geschwindigkeit von sich bewegenden Objekten wird von einer spatiotemporalen Funktion bestimmt, die unabhängig vom Netzwerk und sich darauf bewegenden Objekte ist.
	\item \textbf{Aussage 8} Sich bewegende Objekte gehören einer Klasse an. Die Klasse beschränkt die Maximalgeschwindigkeit des Objekts.
	\item \textbf{Aussage 9} Zeitabhängiger Verkehr (Ankunft/Abfahrt von Fähren) kann die Geschwindikeit und Wege von sich bewegenden Objekten beeinflussen.
\end{itemize}

%\cite{brinkhoff2002framework}
\subsubsection{Startpunkt eines Objekts}
Um die Bewegung von Objekten im Netzwerk darzustellen ist es notwendig, für jedes Objekt einen Startpunkt festzulegen. Der Startpunkt soll ein Knoten im Netzwerk sein. Um diesen zu ermitteln werden drei verschiedene Ansätze vorgestellt:
\begin{itemize}
	\item \textbf{1. Der Datenraumbasierte Ansatz (DSO)}
	Bei diesem Ansatz wird eine xy-Position durch eine 2D-Verteilungsfunktion wie bei \cite{theodoridis1999generation} über den Simulationsraum bestimmt. Darauf wird die Position auf den am nächsten stehenden Knoten des Netzwerks korrigiert. Bei einer Gleichverteilung führt dieser Ansatz dazu, dass an einer dünn besiedelten Stelle mehr Knoten entstehen als bei einer dicht besiedelten, welches der Erfahrung/ Erwartung widerspricht, dass die Nachfrage, in das Netzwerk einzutreten, an dünn besiedelter Stelle gering ist.
	\item \textbf{2. Der Regionbasierte Ansatz (RB)} \label{item:BrinkhoffRegionBased}
	Dieser Ansatz verbessert das Verhalten des Datenraumbasierten Ansatzes dadurch, dass die Verteilung nun auf das Netzwerk direkt anstatt auf den durch das Netzwerk aufgespannten Raum angewendet wird. Dies kann erreicht werden, indem das Netzwerk in unterschiedliche Regionen unterteilt wird, die eine Wahrscheinlichkeit für das Erstellen eines Startknoten angeben. Die Wahrscheinlichkeit kann beispielsweise von der Populationsdichte der Region abgeleitet werden. Somit kann für bestimmte Regionen die Wahrscheinlichkeit für eine Erstellung erhöht oder verringert werden, welches dann Regionabhängig angepasst werden kann.
	\item \textbf{3. Der Netzwerkbasierte Ansatz (NB)}
	Bei diesem Ansatz wird ein Knoten durch eine Verteilung über die möglichen Knoten realisiert. Bei einer Gleichverteilung wird jeder Knoten mit gleicher Wahrscheinlichkeit ausgewählt. Dies führt zu einer Verteilung der Startknoten, die abhängig von der Dichte des Netzwerks ist.
\end{itemize}

%\cite{brinkhoff2002framework}

\subsubsection{Lebenszeit eines Objekts}
Nach Aussage 6 ist die Anzahl der sich bewegenden Objekte eine zeitabhängige Funktion. Um dies zu erfüllen, wird die Erstellung neuer Objekte von einer Maximalschranke kontrolliert, welche die Anzahl neuer Objekt zu einem bestimmten Zeitpunkt berechnet. Ein bewegendes Objekte ``stirbt'' wenn es sein Ziel erreicht.


\subsubsection{Ziel eines Objekts}
Das Ziel eines Objekts kann nicht mit dem selben Ansatz wie beim Startknoten eines Objekt bestimmt werden, da dadurch eine nicht kontrollierbare Verteilung über die Länge der Pfade entsteht. In der realen Welt ist diese Verteilung jedoch an bestimmte Regeln geknüpft. Zum Beispiel ist die Länge eines Pfades abhängig von der Art des bewegenden Objekts (Auto/ Fahrrad / Zu Fuß) oder der Startzeit. Demnach werden unterschiedliche Verteilungen für Start- und Endknoten benötigt.

 Am Beispiel eines Büroarbeiters lässt sich dies verdeutlichen. Hierbei wird der Startknoten abhängig von der Populationsdichte eines Gebietes bestimmt. Das Annehmen der gleichen Verteilung als Ziel würde keinen Sinn ergeben. Mehr Sinn ergibt es die Verteilung abhängig von der Arbeitsplatzdichte zu wählen.

%\cite{brinkhoff2002framework}

\subsubsection{Routenberechnung}
In Aussage zwei wurde definiert, dass ein Objekt überwiegend den schnellsten Weg zu seinem Ziel wählt. Dies kann durch traditionelle Routing-Algorithmen ermöglicht werden. Dabei muss der Routing-Algorithmus jedoch die unterschiedlichen Anforderungen an den Bewegungseigenschaften, wie den präferierten Straßentyp oder die maximale Geschwindigkeit, auf bestimmten Straßentypen berücksichtigen.
Da die Route zu Beginn berechnet wird, wird angenommen, dass die Idealbedingungen gegeben sind, welches ein Widerspruch zu Aussage vier und fünf ist. Eine Lösung dieses Problems wäre das Neuevaluieren der Route zu jedem Zeitstempel. Jedoch würde dies eine hohe Rechenleistung erfordern und nicht der Realität entsprechen. Eine andere Lösung ist eine Neuberechnung beim Eintreten von Ereignissen durchzuführen. Hierbei werden zwei Ereignisse unterschieden:
\begin{itemize}
	\item Das Eintreten eines externen Ereignisses wie zum Beispiel eine Verkehrsmeldung.
	\item Eine starke Abweichung zwischen erwarteter und tatsächlicher Geschwindigkeit, wie bei einem Stau. 
\end{itemize}
Falls eines der Ereignisse eintritt und genug Zeit seit der letzten Neuberechnung verstrichen ist, wird die Situation neu evaluiert.
%\cite{brinkhoff2002framework}

