\section{GraphQL}
\addkeywords{node,edge,graph,where,and,unify,export,as,exhaustive,in,let}
\subsection{Allgemein}
GraphQL ist ein experimenteller Ansatz von Huahai He und Ambuj K. Singh eine Graph-Anfragesprache zu konstruieren.  
Sie bedient sich dem Prinzip von formalen Grammatiken für Sprachen und überträgt dieses auf Graphen. Dieser Abschnitt bezieht sich hauptsächlich auf \cite{singh08querylangGraphDB}.
Zu Beginn werden die wesentlichen Elemente von GraphQL erläutert. An diesen wird erklärt, wie GraphQL vorgeht, um Anfragen zu verarbeiten. 
In Abschnitt \ref{sec:graphqlFLWR} wird GraphQL um FLWR-Ausdrücke erweitert, um unteranderem die Möglichkeit zu bieten, 
Graphen zu transformieren.
\subsubsection{Exkurs: Formale Sprachen}
Um ein gleiches Verständnis von dem folgenden zu haben werden Alphabet, formale Sprachen und Grammatiken definiert.
\begin{definition}[]
	Für eine Menge X ist $X^*$ die Menge der endlichen Folgen über X
\end{definition}
\begin{definition}[Alphabet]
	Ein Alphabet $\Sigma$ ist eine endliche nichtleere Menge
\end{definition}
\begin{definition}[Grammatik]
	Eine Grammatik $G$ ist ein 4-Tupel $G=(V, \Sigma, P,S)$, das folgende Bedingungen erfüllt:\\
	\begin{enumerate}
		\item V ist eine endliche Menge von Nicht-Terminalen oder Variablen.\\
		\item $\Sigma$ ist ein (endliches) Alphabet (Menge der Terminale) mit $V \cap \Sigma= \emptyset$\\
		\item $P \subseteq (V \cup \Sigma)^+ \times (V \cup \Sigma)^*$ ist eine endliche Menge von Regeln oder Produktionen.\\
		\item $S \in V$ ist die Startvariable.
	\end{enumerate}
\end{definition}
\begin{definition}[Formale Sprachen]
	\begin{enumerate}
		\item Sei $\Sigma$ ein Alphabet. Teilmengen von $\Sigma^*$ werden (formale) Sprachen über $\Sigma$ genannt.\\
		\item Eine Menge $L$ ist eine Sprache, wenn es ein Alphabet $\Sigma$ gibt, so dass L Sprache über $\Sigma$ ist (d.h. so dass $L \subseteq \Sigma^*$) 
	\end{enumerate}
\end{definition}


\subsection{Datenmodell}
Die Basiseinheit von GraphQL sind Graphen. Es ist jedoch nicht genau definiert, ob es sich hierbei um gerichtete oder ungerichtetete Graphen handelt. 
In \cite{singh08querylangGraphDB} gibt es auf beides Hinweise. Im weiteren Verlauf wird daher von gerichteten Graphen ausgegangen.
Die Graphen werden in einfache und komplexe Graph-Muster unterschieden.
Einfache Graph-Muster sind Graphen die nur Knoten und Kanten beinhalten. Knoten und Kanten sind die Terminale der Grammatik.
Komplexe Graph-Muster können zusätzlich Graph-Muster beinhalten.  
Graph-Muster sind demnach die Nicht-Terminale der Grammatik. Graph-Muster, sowie Knoten und Kanten können auf Attribute 
abbilden, welche wiederum auf die Daten abbilden. Des Weiteren können Knoten, Kanten und Graph-Muster einen Tag besitzen, der die Art des 
Elements beschreibt. Eine Graph-Muster-Definition des Beispielgraph würde in GraphQL wie folgt aussehen:
\begin{lstlisting}[caption={Einfaches Graph-Muster},label={lst:graphqlDatagraph}]
graph G <propertygraph>{(*@\label{lst:graphqlDatagraphPropertygraph}@*)
	node $v_1$ <person name="marko" age=29>;(*@\label{lst:graphqlDatagraphNode}@*)
	node $v_2$ <person name="vadas" age=27>;
	node $v_3$ <creation name="lop" lang="java">;
	node $v_4$ <person name="josh" age=32>;
	node $v_5$ <creation name="ripple" lang="java">;
	node $v_6$ <person name="peter" age=35>;
	edge $e_1(v_1,v_2)$ <knows weight=0.5>;(*@\label{lst:graphqlDatagraphEdge}@*)
	edge $e_2(v_1,v_4)$ <knows weight=1.0>;
	edge $e_3(v_1,v_3)$ <created weight=0.4>;
	edge $e_4(v_4,v_5)$ <created weight=1.0>;
	edge $e_5(v_4,v_3)$ <created weight=0.4>;
	edge $e_6(v_6,v_3)$ <created weight=0.2>;
}

\end{lstlisting}

In Zeile \ref{lst:graphqlDatagraphPropertygraph} wird der Graph G vom Typ \textit{propertygraph} definiert.\\
Darauf folgt die Definition der Knoten und Kanten dieses Graphen. Der Knoten $v_1$ (Zeile \ref{lst:graphqlDatagraphNode}) 
definiert einen Knoten vom Typ \textit{Person} mit den Attributen \textit{name} und \textit{age}. $v_1$ ist die ID des Knotens und sollte eindeutig sein.\\
Die Kanten (Zeile \ref{lst:graphqlDatagraphEdge}) werden so definiert, dass erst die ID der Kante genannt wird. Anschließend folgt,
welcher Knoten Quell- und welcher Knoten Zielknoten ist. (QuellknotenID, ZielknotenID) 
Danach werden die Attribute der Kante wie bei den Knoten definiert. 
\begin{figure}[H]
\centering
\includegraphics[width=0.75\linewidth]{pics/querylang/graphql/datagraph.pdf}
\captionsource{Darstellung des Graphen aus Listing \ref{lst:graphqlDatagraph}}{Eigene Darstellung}
\label{fig:graphqlDatagraph}
\end{figure} 
Im Folgenden wird beschrieben, auf welche Arten einfache Graph-Muster durch komplexe Graph-Muster verbunden werden 
können, um neue einfache Graph-Muster zu erstellen. Als Beispiel für die Verbindungen wird dieses einfache Graph-Muster genutzt. Dieses und die Folgenden Graph-Muster
aus diesem Abschnitt wurden von \cite[Abbildung 3]{singh08querylangGraphDB} übernommen.\\
\begin{center}
\begin{minipage}{0.4\textwidth}
\begin{lstlisting}
graph $G_1${
	node $v_1, v_2, v_3$;
	edge $e_1(v_1,v_2)$;
	edge $e_2 (v_2,v_3)$;
	edge $e_3 (v_3,v_1)$;
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.3\textwidth}
\includegraphics[width=\textwidth]{pics/querylang/graphql/graphMotifExample}
\end{minipage}

\end{center}
\subsubsection{Konkatenation}
Ein Graph-Muster kann aus anderen Graph-Mustern hervorgehen, allerdings muss dabei explizit definiert werden, an welchen Knoten die Graph-Muster sich 
verbinden, um eine eindeutige Definition zu gewährleisten.\\
Es gibt zwei Möglichkeiten eine Konkatenation von Graph-Mustern durchzuführen.
\begin{enumerate}%Vor-/ Nachteile der jeweiligen Anwendung?
	\item Man verbindet die Knoten an der Stelle an der sie verbunden werden sollen mit einer neuen Kante.\\
	\begin{center}
\begin{minipage}{0.4\textwidth}
\begin{lstlisting}
graph $G_2${
	graph $G_1$ as X;
	graph $G_1$ as Y; 
	edge $e_4$(X.$v_1$,Y.$v_1$);
	edge $e_5$ (X.$v_3$,Y.$v_2$);
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\includegraphics[width=\textwidth]{pics/querylang/graphql/graphMotifConcat1}
\end{minipage}
\end{center}

	\item Man verschmelzt die beiden Verbindungsknoten zu einem Knoten.\\
	\begin{center}
	\begin{minipage}{0.4\textwidth}
\begin{lstlisting}
graph $G_3${
	graph $G_1$ as X;
	graph $G_1$ as Y; 
	unify X.$v_1$,Y.$v_1$;
	unify X.$v_3$,Y.$v_2$;
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\includegraphics[width=\textwidth]{pics/querylang/graphql/graphMotifConcat2}
\end{minipage}
\end{center}
\end{enumerate}

\subsubsection{Disjunktion}
	\begin{center}
	\begin{minipage}{0.4\textwidth}
\begin{lstlisting}
graph $G_4${
	node $v_1,v_2$;
	edge $e_1(v_1,v_2)$;
	{
		node $v_3$;
		edge $e_2(v_1,v_3)$;
		edge $e_3(v_2,v_3)$;
	} |{
		node $v_3,v_4$
		edge $e_2(v_1,v_3)$;
		edge $e_3(v_2,v_4)$;
		edge $e_4(v_3,v_4)$;
	}
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.5\textwidth}
\includegraphics[width=\textwidth]{pics/querylang/graphql/graphMotifDisjunction}
\end{minipage}
\end{center}

\subsubsection{Rekursion/Kleene Stern}
In formalen Sprachen gibt es die Möglichkeit der Rekursion, welche wie folgt definiert ist: $$S \rightarrow S_1S|\varepsilon$$
Dieses Vorgehen ist in Relationalen-Datenbanken nicht vorgesehen. Um die Rekursion auf GraphQL zu übertragen, kann ein Graph-Muster  sich
direkt oder indirekt(über eine Kette von anderen Graph-Mustern) selbst aufrufen. Im Detail wird dies ermöglicht, indem 
ein Graph-Muster sich im einfachsten Fall(Selbstrekursion) wieder selbst aufruft.Dabei wird der Verbindungsknoten aus 
dem neuen Graph-Muster in das aktuelle Graph-Muster exportiert. Das Schlüsselwort \textbf{export} erstellt einen neuen Knoten und vereinigt ihn mit dem
Verbindungsknoten aus dem Sub-Graphen. Dieses Vorgehen ist auch mit kompletten Graph-Mustern möglich.
\\
\\
\begin{center}
\begin{minipage}{0.4\textwidth}
\begin{lstlisting}
graph Path{
	graph Path;
	node $v_1$;
	edge $e_1$($v_1$, Path.$v_1$)
	export Path.$v_2$ as $v_2$
} | {
	node $v_1$,$v_2$;
	edge $e_1$($v_1$,$v_2$)
}
\end{lstlisting}
\begin{lstlisting}
graph Cycle{
	graph Path;
	edge $e_1$(Path.$v_1$, Path.$v_2$);
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.5\textwidth}
	\includegraphics[width=\textwidth]{pics/querylang/graphql/graphMotifRecursion}
\end{minipage}
\end{center}
\subsection{Graph-Pattern}
Ein Graph-Pattern ist ein Graph-Muster mit einem Ausdruck über diesem. Der Ausdruck kann eine Kombination aus Boolischen und Arithmetischen Vergleichsoperationen sein.
Des Weiteren muss ein Ausdruck nicht zwangsweise für ein Graph-Muster im Allgemeinen gelten, sondern kann auch einzelne Bedingungen für Knoten, Kanten und Graphen innerhalb des Graph-Musters beschreiben.
\begin{definition}[Graph-Pattern]
	Ein Graph-Pattern ist ein Paar $\mathcal{P}=(\mathcal{M},\mathcal{F})$, wobei $\mathcal{M}$ ein Graph-Muster, und $\mathcal{F}$ 
	ein Ausdruck über den Attributen des Graph-Musters ist.
\end{definition}
\begin{center}
\begin{minipage}{0.4\textwidth}
\begin{lstlisting}[caption={einfaches Graph-Pattern},label={lst:graphqlGraphPattern}]
graph P{
	node $v_1$ <person>;
	node $v_2$ <person>;
	edge $e_1(v_1,v_2)$ <knows>;
} where $v_1.name$="marko";
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\includegraphics[width=\textwidth]{pics/querylang/graphql/graphPattern}
\end{minipage}
\end{center}
Mit dem Graph-Pattern aus Listing \ref{lst:graphqlGraphPattern} kann der Beispielgraph (Listing \ref{lst:graphqlDatagraph}) 
angefragt werden. Falls das Graph-Pattern innerhalb des Beispielgraphs eine Übereinstimmung hat, erhält man eine Menge von 
\textit{Mappings}, die angeben, wie das Graph-Pattern auf den Graph \textit{gemappt} werden kann. 

\begin{definition}[Graph-Pattern-Matching]\label{def:graphqlGraphPatternMatching} Ein Graph-Pattern $\mathcal{P}(\mathcal{M},\mathcal{F})$ matched einen Graph 
wenn ein injektives \textit{Mapping} $\phi: V(\mathcal{M})\rightarrow V(\mathcal{G})$ existiert, 
so dass gilt $\forall e(u,v) \in E(\mathcal{M}), (\phi(u),\phi(v))$ ist eine Kante in $G$, welches den Ausdruck $\mathcal{F}_{\phi}(G)$ erfüllt.
\end{definition}

Ein Mapping für Graph $G$(Listing \ref{lst:graphqlDatagraph}) und Graph-Pattern $P$(Listing \ref{lst:graphqlGraphPattern}) wäre folgendes:
\begin{figure}[H]
\begin{lstlisting}[caption={Mapping für Graph G(Listing \ref{lst:graphqlDatagraph}) und Graph-Pattern $P$(Listing \ref{lst:graphqlGraphPattern})}]
Mapping $\phi$:
	$\phi(P.v_1)\rightarrow G.v_1$
	$\phi(P.v_2)\rightarrow G.v_2$
	$\phi(P.e_1(v_1,v_2))\rightarrow G.e_1(G.v_1,v_2)$
\end{lstlisting}
\end{figure}
Aus einem Mapping resultiert ein Matched-Graph.
\begin{definition}[Matched Graph]
\label{def:graphqlMatchedGraph}
Wenn zu einem Graph-Pattern $\mathcal{P}$ und einem Graph $G$ ein Mapping $\phi$ existiert.
 So wird das Tripel $\big \langle \phi, \mathcal{P},G\big \rangle$ als Matched-Graph bezeichnet 
und mit $\phi_{\mathcal{P}}(G)$ gekennzeichnet.
\end{definition}
Ein Matched-Graph ist wieder ein einfaches Graph-Muster, auf das weitere Graph-Pattern angewendet werden können.
Der  \textit{gematchte} Graph in unserem Beispiel würde wie folgt aussehen:
\begin{center}
\begin{minipage}{0.58\textwidth}
\begin{lstlisting}[caption={Matched-Graph },label={lst:graphqlMatchedGraph}]
graph $\phi_{\mathcal{P}}(G)${
	node $v_1$ <person name="marko" age=29>;
	node $v_2$ <person name="vadas" age=27>;
	edge $e_1(v_1,v_2)$ <knows weight=0.5>;
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\includegraphics[width=\textwidth]{pics/querylang/graphql/matchedGraph}
\end{minipage}
\end{center}
 \subsection{Graph Algebra}
 \subsubsection{Selektion}
 Die Selektion($\sigma$), wie sie innerhalb der Relationalen-Algebra bekannt ist, wird mittels des \hyperref[def:graphqlGraphPatternMatching]{Graph-Pattern-Matching} durchgeführt. 
Die Eingabe $\mathcal{C}$ ist dabei eine Menge von Graphen.  Dabei kann ein Graph-Pattern innerhalb eines Graphs mehrere Treffer haben und somit eine Menge von Graph-Mappings 
 zurückgeben. Es gibt dabei jedoch die Option \textit{exhaustive} welche angibt, ob ein oder alle Mappings zurückgegeben 
 werden sollen. Die Ausgabe ist eine Menge von \hyperref[def:graphqlMatchedGraph]{Matched-Graphs}
 $$\sigma_{\mathcal{P}}(\mathcal{C})= \{\phi_{\mathcal{P}}(G)\mid G \in \mathcal{C}\}$$
 \subsubsection{Kartesisches Produkt}
 Das Kartesische-Produkt vereint zwei Graph-Mengen $(\mathcal{C}, \mathcal{D})$ miteinander und gibt eine Graph-Menge zurück. Die jeweiligen Elemente 
 aus den Graph-Mengen sind jedoch nicht verbunden.
 $$\mathcal{C} \times \mathcal{D}= \{ \textbf{graph} \{\textbf{graph} \  G_1,G_2;\} \mid G_1 \in \mathcal{C}, G_2 \in \mathcal{D}\}$$
 \subsubsection{Join}
 Es gibt in GraphQL zwei Arten von Joins. Einerseits gibt es den Join über einen Ausdruck der zu bildenen Graph-Tupel bei der Anwendung des 
 Kartesischen-Produktes. Zum Beispiel: $G_1.name=G_2.name$ -  dies wird als \textit{Valued-Join} bezeichnet. Der 
 resultierende Graph würde nun jedoch weiterhin eine Menge von nicht verbundenen Graphen $G_1 \in \mathcal{C}, G_2 \in 
 \mathcal{D}$ ausgeben. Beide haben mindestens das Attribut \textit{name} gemein. Wenn man andererseits die Graphen über 
 diesen Punkt verbinden möchte, spricht man von einem \textit{structural-join}. Dies wird über den Verkettungs-Operator gelöst.
 
 \subsubsection{Verkettung}
 Der Verkettungs-Operator $\omega$ verbindet \hyperref[def:graphqlMatchedGraph]{Matched-Graphs} über ein \textit{Graph-Template} zu einem neuen Graphen. 
 Wie dieser neue Graph aussehen soll, wird im Graph-Template definiert. 
 \begin{definition}[Graph-Template]
 Ein Graph-Template $\mathcal{T}$ besteht aus einer Menge von Graph-Pattern und einem Template-Body 
 der den neuen Graph mit Elementen aus den vorhergegangenen Graph-Pattern beschreibt.
 \end{definition}  
 \begin{center}
 \begin{lstlisting}[caption={Graph Template für einen neuen Graphen},label={lst:graphqlGraphTemplate}]
 $\mathcal{T}_\mathcal{P}$ graph {
 	node $v_1$ <person name="$P.v_1.name$" age="$P.v_1.age$">;
 	node $v_2$ <person name="$P.v_2.name$" age="$P.v_2.age$">;
 	node $v_3$ <person name="Bob" age=26>;
 	edge $e_1(v_1,v_2)$ <knows since=2014>;
 	edge $e_2(v_2,v_3)$ <knows since=2009>;
 	edge $e_3(v_1, v_3)$ <knows>;
 }
 \end{lstlisting}
\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{pics/querylang/graphql/graphTemplate}
\captionsource{Darstellung des Graph-Template aus Listing {\ref{lst:graphqlGraphTemplate}}}{Eigene Darstellung}
\end{figure} 

\end{center}
Der aus Listing \ref{lst:graphqlGraphTemplate} resultierende Graph würde wie folgt aussehen:
 \begin{center}
 \begin{lstlisting}[caption={Graph Instanz aus Graph-Pattern $\mathcal{P}$ (Listing \ref{lst:graphqlGraphPattern}) und Template $\mathcal{T}_\mathcal{P}$ (Listing \ref{lst:graphqlGraphTemplate})},label={lst:graphqlGraphTemplateInstance}]
 $\mathcal{T}_\mathcal{P}$ graph {
 	node $v_1$ <person name="marko" age="29">;
 	node $v_2$ <person name="vadas" age="27">;
 	node $v_3$ <person name="Bob" age=26>;
 	edge $e_1(v_1,v_2)$ <knows since=2014>;
 	edge $e_2(v_2,v_3)$ <knows since=2009>;
 	edge $e_3(v_1, v_3)$ <knows>;
 }
 \end{lstlisting}
\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{pics/querylang/graphql/graphTemplateInstance}
\captionsource{Darstellung der Graph-Instanz aus Listing {\ref{lst:graphqlGraphTemplateInstance}}}{Eigene Darstellung}
\label{fig:graphqlGraphTemplateInstance}
\end{figure} 
\end{center}
 Wenn man nun die Graphen vor (Listing \ref{lst:graphqlMatchedGraph}) und nach der Verkettung(Listing \ref{lst:graphqlGraphTemplateInstance}) 
 vergleicht, sieht man, dass man mit Leichtigkeit jede Art von Graph manipulieren kann. \\
 
  Der Verkettungs-Operator $\omega$ führt die Manipulation von Graph-Mengen $\mathcal{C}$ ein. Dabei wird auf jedes Element der Menge 
  das Graph-Template $\mathcal{T}_\mathcal{P}$ angewendet und der Ergebnis-Menge hinzugefügt.
 $$\omega_{\mathcal{T}_\mathcal{P}}(\mathcal{C})= \{\mathcal{T}_\mathcal{P}(G)\mid G \in \mathcal{C}\}$$
 Dies funktioniert auch mit zwei oder einer größeren Anzahl von Mengen über das Kartesische-Produkt
 $$\omega_{\mathcal{T}_{\mathcal{P}_1,\mathcal{P}_2}}(\mathcal{C}_1,\mathcal{C}_2)= \omega_{\mathcal{T}_{\mathcal{P}}}(\mathcal{C}_1\times \mathcal{C}_2) \ mit \ \mathcal{P}= \textbf{graph} \{ \textbf{graph} \ \mathcal{P}_1,\mathcal{P}_2\}$$
 
\subsection{Spezielle Anfrage-Pattern}\label{sec:graphqlFLWR}
In GraphQL können Anfragen über die von XQuery bekannten FLWR(For, Let, Where, Return)-Ausdrücke realisiert werden.
Dabei wird jedes Vorkommen eines Graph-Pattern in einem Graphen \textit{gematcht}. Die daraus resultierenden Matched-Graphs können im Let-Teil manipuliert, und zu einem neuen Graphen
verbunden werden. Das Graph-Pattern sieht wie folgt aus:
\begin{center}
\begin{minipage}{0.5\textwidth}
\begin{lstlisting}[caption={Graph-Pattern für FLWR-Anfrage},label={lst:graphqlPatternFLWR}]
graph P {
	node $v_1$ <person>;
	node $v_2$;
	edge $e_1(v_1,v_2)$;
}
\end{lstlisting}
\end{minipage}
\begin{minipage}{0.4\textwidth}
\includegraphics[width=\textwidth]{pics/querylang/graphql/patternFLWR}
\end{minipage}
\end{center}
Dieses Graph-Pattern soll in dem FLWR-Ausdruck(Listing \ref{lst:graphqlFLWR}) in dem Graph aus Listing \ref{lst:graphqlFLWRh} nach Matches suchen und 
mit diesen eine Graph-Transformation durchführen, sodass ein neuer Graph entsteht. Dazu wird zunächst Graph H vorgestellt. 
\begin{lstlisting}[caption={Graph H für FLWR-Anfrage},label={lst:graphqlFLWRh}]
graph H{
	node $v_1$ <person name="marko" surname="rodriguez">
	node $v_2$ <duck name="Daisy" surname="Duck">
	node $v_3$ <duck nam="Donald" surname="Duck">
	node $v_4$ <duck name="Dagorbert" surname="Duck">
	node $v_5$ <creation name="ripple">
	node $v_6$ <person name="peter" surname="neubauer">
	node $v_7$ <person name="Tom" surname="Tailor">
	edge $e_1(v_1,v_2)$ <knows>;
	edge $e_2(v_2,v_4)$ <knows>;
	edge $e_3(v_3,v_4)$ <knows>;
	edge $e_4(v_3,v_2)$ <knows>;
	edge $e_5(v_6,v_5)$ <likes>;
	edge $e_6(v_7,v_5)$ <likes>;
	edge $e_7(v_7,v_6)$ <knows>;
}
\end{lstlisting}

\begin{figure}[H]
\centering
\includegraphics[width=0.75\linewidth]{pics/querylang/graphql/graphqlFLWRh.pdf}
\captionsource{Darstellung des Graphen aus Listing \ref{lst:graphqlFLWRh}}{Eigene Darstellung}
\label{fig:graphqlFLWRh}
\end{figure} 
\newpage
Letztendlich kann der FLWR-Ausdruck beschrieben werden. 
\begin{lstlisting}[caption={FLWR Ausdruck mit Pattern P auf Graph G und H},label={lst:graphqlFLWR}]
	C := graph G (Listing (*@\ref{lst:graphqlDatagraph}@*));
	for P exhaustive in H;
	let C := graph {
		graph C;
		node P.$v_1$, P.$v_2$;
		edge P.$e_1$(P.$v_1$, P.$v_2$);
		unify P.$v_1$, C.$v_1$ where P.$v_1$.name=C.$v_1$.name;
		unify P.$v_2$, C.$v_2$ where P.$v_2$.name=C.$v_2$.name
	}
\end{lstlisting}
Der Resultierende Graph wird in Graph C gespeichert. Der For-Ausdruck gibt dabei an, dass das Pattern P (Listing \ref{lst:graphqlPatternFLWR}) 
in Graph H (Listing \ref{lst:graphqlFLWRh}) gefunden werden soll. Die Option \textit{exhaustive} beschreibt, dass 
jedes Mapping gefunden werden soll. Im Let-Teil wird jeder Matched-Graph in den Graph C eingefügt und danach 
mit \textit{unify} über das Attribut \textit{name} mit bereits existenten Knoten vereinigt. Dies ist ein inkrementeller Prozess.
Der resultierende Graph sieht wie folgt aus:

\begin{figure}[H]
\centering
\includegraphics[width=0.75\linewidth]{pics/querylang/graphql/graphqlFLWR.pdf}
\captionsource{Aus Listing \ref{lst:graphqlFLWR} resultierender Graph}{Eigene Darstellung}%\ref{lst:graphqlFLWRh}}{}
\label{fig:graphqlFLWR}
\end{figure} 
Wenn man Graph G mit dem Ergebnis Graph C vergleicht, erkennt man, dass nur die Elemente, die dem Pattern entsprechen, Teil des neuen Graphen wurden.
Durch das \textit{unify} wurden die Informationen der Knoten zusammengefasst. Dadurch kann man gut mit semistrukturierten Daten arbeiten.

